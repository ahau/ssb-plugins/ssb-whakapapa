const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { promisify } = require('util')
const pick = require('lodash.pick')

const CrutByType = require('../lib/crut-by-type')
const CrutById = require('../lib/crut-by-id')
const fixDetails = require('../lib/fix-details')

module.exports = function Link (ssb) {
  const crutByType = CrutByType(ssb)
  const crutById = CrutById(ssb, crutByType)

  function get (id, cb) {
    if (cb === undefined) return promisify(get)(id)

    crutById(id, (err, crut) => {
      if (err) return cb(err)

      crut.read(id, cb)
    })
  }

  function update (id, details, cb) {
    if (cb === undefined) return promisify(update)(id, details)

    crutById(id, (err, crut) => {
      if (err) return cb(err)

      crut.forceUpdate(id, fixDetails(details), cb)
    })
  }

  function tombstone (id, details, cb) {
    if (cb === undefined) return promisify(tombstone)(id, details)

    crutById(id, (err, crut) => {
      if (err) return cb(err)

      crut.forceTombstone(id, fixDetails(details), cb)
    })
  }

  function getLinksOfType (id, type, cb) {
    if (cb === undefined) return promisify(getLinksOfType)(id, type)

    const crut = crutByType(type)
    if (!crut) return cb(new Error('invalid type ' + type))

    const result = {
      id,
      parentLinks: [],
      childLinks: []
    }

    pull(
      pullLinks(id, type),
      pull.map((msg) => flattenLink(msg, crut)),
      pull.through(link => {
        if (link.parent === id) {
          link.id = link.child
          result.childLinks.push(link)
        } else if (link.child === id) {
          link.id = link.parent
          result.parentLinks.push(link)
        }
      }),
      pull.collect(err => {
        if (err) return cb(err)
        cb(null, result)
      })
    )
  }

  return {
    create ({ type, parent, child }, details = {}, cb) {
      const crut = crutByType(type)
      if (!crut) return cb(new Error('invalid type ' + type))

      return crut.create({ parent, child, ...fixDetails(details) }, cb)
    },
    get,
    update,
    tombstone,

    list (type, opts, cb) {
      const crut = crutByType(type)
      return crut.list(opts, cb)
    },
    getLinksOfType

  }

  function pullLinks (id, msgType) {
    const crut = crutByType(msgType)

    const { where, type, toPullStream } = ssb.db.operators

    return pull(
      ssb.db.query(
        where(type(msgType)),
        toPullStream()
      ),
      pull.filter(m => (
        m.value.content.parent === id ||
        m.value.content.child === id
      )),
      pull.filter(crut.spec.isRoot),

      paraMap(
        (root, cb) => {
          crut.read(root.key, (err, link) => {
            if (err) return cb(null, null) // HACK - drop any problems
            cb(null, link)
          })
        },
        5
      ),
      pull.filter(Boolean) // drop the error/ null cases
    )
  }
}

function flattenLink (link, crut) {
  if (link.states.length > 1) {
    console.error('links should not currently have more than one state as we are only permitting one author!')
  }

  return {
    ...pick(link, crut.strategy.fields),
    linkId: link.key,
    parent: link.parent,
    child: link.child,
    recps: link.recps
  }
}
