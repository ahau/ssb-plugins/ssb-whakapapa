const Server = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  // }

  const stack = Server

  stack
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-box'))
    .use(require('../')) // ssb-whakapapa

  return stack(opts)
}
