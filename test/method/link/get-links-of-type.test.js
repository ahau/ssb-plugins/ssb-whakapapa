const test = require('tape')
const Server = require('../../test-bot')
const pull = require('pull-stream')
const { promisify: p } = require('util')

test('link/getLinksOfType/story', t => {
  t.plan(13)
  const server = Server()
  const date = Date.now()

  const story = '%story/XajjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const artefact1 = '%artefact1/werwerwerSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const profile = '%profile/jjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const story2 = '%story2/eXPu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256'

  const links = [
    {
      type: 'link/story-artefact',
      parent: story,
      child: artefact1
    },
    {
      type: 'link/story-profile/mention',
      parent: story,
      child: profile
    },
    {
      type: 'link/story-profile/contributor',
      parent: story,
      child: profile
    },
    {
      type: 'link/story-story',
      parent: story,
      child: story2
    }
  ]

  pull(
    pull.values(links),
    pull.asyncMap(({ type, parent, child, details }, cb) => {
      server.whakapapa.link.create({ type, parent, child }, details, cb)
    }),
    pull.collect((err, ids) => {
      t.false(err, 'should be no create errors')
      let processing = 0

      ids.forEach((id, i) => {
        processing++
        const link = links[i]

        server.whakapapa.link.getLinksOfType(story, link.type, (err, data) => {
          t.false(err, link.type + ' get should not return any errors')

          const expected1 = {
            id: story,
            parentLinks: [],
            childLinks: [
              {
                id: link.child,
                linkId: id,
                tombstone: null,

                parent: links[i].parent,
                child: links[i].child,
                recps: null
              }
            ]
          }

          t.deepEqual(data, expected1, 'gets reduced state of ' + link.type)

          const update = {
            tombstone: {
              set: {
                date: date,
                reason: 'duplication'
              }
            }
          }

          if (i === 2) update.recps = [id] // test recps on the last one

          server.whakapapa.link.update(id, update, (err, res) => {
            const expected2 = expected1
            if (i === 2 && err) {
              // see if the last links update called back an error for the recps
              t.deepEqual(err, new Error('Cannot update recps field. Please check the details provided'), 'throws error on update recps ' + link.type)
              // this means the update wouldnt of been worked
              processing--
              return
            }

            if (err) return t.error(err, 'this should not have errored')
            expected2.childLinks[0].tombstone = update.tombstone
            server.whakapapa.link.getLinksOfType(story, link.type, (err, data) => {
              if (err) throw err
              t.deepEqual(data, expected2, 'gets (updated) reduced state of ' + link.type)
              processing--
              if (processing === 0) server.close()
            })
          })
        })
      })
    })
  )
})

test('link/getLinksOfType/whakapapa', async t => {
  const server = Server()

  const mum = '%parentA/jjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256'
  const dad = '%parentB/wndSuf93EMjjIn2UkDK7AvU00kQt/3qxehc=.sha256'
  const stepMum = '%parentC/wndSuf93EMjjIn2UkDK7AvU00kQt/3qxehc=.sha256'

  const child1 = '%child1/kdSuDwnf93BjjIn2UEMK7AvU00kQt/3qxehc=.sha256'
  const child2 = '%child2/kdSuDwnf93BjjIn2UEMK7AvU00kQt/3qxehc=.sha256'

  const child3 = '%child3/kdSuDwnf93BjjIn2UEMK7AvU00kQt/3qxehc=.sha256'

  const getLinks = (id, type) => p(server.whakapapa.link.getLinksOfType)(id, type)

  const whakapapaLinks = [
    { // mum and dad got together
      type: 'link/profile-profile/partner',
      parent: dad,
      child: mum,
      details: {
        relationshipType: {
          set: 'married'
        }
      }
    },
    { // they had a child together
      type: 'link/profile-profile/child',
      parent: mum,
      child: child1,
      details: {
        relationshipType: {
          set: 'birth'
        }
      }
    },
    {
      type: 'link/profile-profile/child',
      parent: dad,
      child: child1,
      details: {
        relationshipType: {
          set: 'birth'
        }
      }
    },
    { // after they separated, dad remarried
      type: 'link/profile-profile/partner',
      parent: stepMum, // here i will set the stepMukm
      child: dad,
      details: {
        relationshipType: {
          set: 'partners'
        }
      }
    },
    { // dad and the new partner had a child
      type: 'link/profile-profile/child',
      parent: dad,
      child: child2,
      details: {
        relationshipType: {
          set: 'birth'
        }
      }
    },
    { // the also decided to whangai another child
      type: 'link/profile-profile/child',
      parent: dad,
      child: child3,
      details: {
        relationshipType: {
          set: 'whangai'
        }
      }
    }
  ]

  // create all the links

  pull(
    pull.values(whakapapaLinks),
    pull.asyncMap(({ type, parent, child, details }, cb) => {
      server.whakapapa.link.create({ type, parent, child }, details, cb)
    }),
    pull.collect(async (err, ids) => {
      t.error(err, 'creates all links without error')

      // get the dads partners
      const partnerLinks = await getLinks(dad, 'link/profile-profile/partner')
      const partners = [...partnerLinks.parentLinks, ...partnerLinks.childLinks]

      t.deepEqual(
        partners,
        [
          {
            id: stepMum,
            linkId: ids[3],
            tombstone: null,

            parent: stepMum,
            child: dad,
            relationshipType: 'partners',
            recps: null
          },
          { // should see the mum and step mum here
            id: mum,
            linkId: ids[0],
            tombstone: null,

            parent: dad,
            child: mum,
            relationshipType: 'married',
            recps: null
          }
        ],
        'returns the correct partners of dad'
      )

      // get the dads children
      const { childLinks: children } = await getLinks(dad, 'link/profile-profile/child')

      t.deepEqual(
        children,
        [
          {
            id: child1,
            linkId: ids[2],
            relationshipType: 'birth',
            legallyAdopted: null,
            tombstone: null,

            parent: dad,
            child: child1,
            recps: null
          },
          {
            id: child2,
            linkId: ids[4],
            relationshipType: 'birth',
            legallyAdopted: null,
            tombstone: null,

            parent: dad,
            child: child2,
            recps: null
          },
          {
            id: child3,
            linkId: ids[5],
            relationshipType: 'whangai',
            legallyAdopted: null,
            tombstone: null,

            parent: dad,
            child: child3,
            recps: null
          }
        ],
        'returns the correct children of dad'
      )

      // check child3's parents
      const { parentLinks: parents } = await getLinks(child3, 'link/profile-profile/child')
      t.deepEqual(
        parents,
        [
          {
            id: dad,
            linkId: ids[5],
            relationshipType: 'whangai',
            legallyAdopted: null,
            tombstone: null,

            parent: dad,
            child: child3,
            recps: null
          }
        ],
        'returns the correct parents of one of the children'
      )

      server.close()
      t.end()
    })
  )
})
