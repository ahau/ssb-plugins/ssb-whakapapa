const test = require('tape')
const { isMsg } = require('ssb-ref')
const Server = require('../../test-bot')

const type = 'link/story-artefact'
const parent = '%story/lljjIn2UkDwndSuf93EMK7AvU00kQt/3qxehc=.sha256' // a story id
const child = '%artefact/Pu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256' // an artefact id

test('link/create (success)', t => {
  t.plan(3)
  const server = Server()

  const details = {
    recps: [
      server.id, // me
      '@2RNGJafZtMHzd76gyvqH6EJiK7kBnXeak5mBWzoO/iU=.ed25519'
    ]
  }

  server.whakapapa.link.create({ type, parent, child }, details, (err, linkId) => {
    t.false(err, type + ' creates without error')

    t.true(isMsg(linkId), 'calls back with a messageId for link')

    const expected = {
      type,
      parent,
      child,
      recps: details.recps,
      tangles: { link: { root: null, previous: null } }
    }

    server.get({ id: linkId, private: true }, (_, value) => {
      t.deepEqual(value.content, expected, 'create returns expected result')
      server.close()
    })
  })
})

test('link/create (link failure)', t => {
  t.plan(2)
  function create (parent, child, expectedErr, msg) {
    const server = new Server()
    server.whakapapa.link.create({ type, parent, child }, null, (err) => {
      t.equal(err.message, expectedErr, msg)

      server.close()
    })
  }

  const link = '%bazdatAeXPu81tBueITsLYss0vLUdVA7VQa55P21qkc=.sha256'

  create('link/story-test', link, 'data.parent referenced schema does not match', 'parent failure')
  create(link, 'test', 'data.child referenced schema does not match', 'child failure')
})

test('link/create (type)', t => {
  t.plan(2)
  function create (type, expectedErr, displayMsg) {
    const server = new Server()
    server.whakapapa.link.create({ type, parent, child }, null, (err, msg) => {
      if (expectedErr) {
        t.equal(err.message, expectedErr, displayMsg)
        server.close()
        return
      }

      t.true(msg, displayMsg)
      server.close()
    })
  }

  create('randomType', 'invalid type randomType', 'non existent type fails')
  create(type, null, 'accepts type')
})
