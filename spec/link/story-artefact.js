module.exports = {
  type: 'link/story-artefact',
  staticProps: {
    child: { $ref: '#/definitions/messageId', required: true },
    parent: { $ref: '#/definitions/messageId', required: true }
  },
  props: {}
}
