const Overwrite = require('@tangle/overwrite')

module.exports = {
  type: 'link/profile-profile/partner',
  staticProps: {
    child: { $ref: '#/definitions/messageId', required: true },
    parent: { $ref: '#/definitions/messageId', required: true }
  },
  props: {
    relationshipType: Overwrite({
      enum: ['partners', 'married', 'divorced', 'unknown']
    })
  }
}
