const Crut = require('ssb-crut')
// TODO - consider using ssb-crut-authors
const Force = require('ssb-crut/lib/force')

const childSpec = require('../spec/link/profile-profile/child')
const partnerSpec = require('../spec/link/profile-profile/partner')

const contributorSpec = require('../spec/link/story-profile/contributor')
const creatorSpec = require('../spec/link/story-profile/creator')
const mentionSpec = require('../spec/link/story-profile/mention')

const storyArtefactSpec = require('../spec/link/story-artefact')
const storyStorySpec = require('../spec/link/story-story')

module.exports = function CrutByType (ssb) {
  const child = Crutify(childSpec)
  const partner = Crutify(partnerSpec)

  const contributor = Crutify(contributorSpec)
  const creator = Crutify(creatorSpec)
  const mention = Crutify(mentionSpec)

  const storyArtefact = Crutify(storyArtefactSpec)
  const storyStory = Crutify(storyStorySpec)

  return function crutByType (type) {
    switch (type) {
      case 'link/profile-profile/child': return child
      case 'link/profile-profile/partner': return partner

      case 'link/story-profile/contributor': return contributor
      case 'link/story-profile/creator': return creator
      case 'link/story-profile/mention': return mention

      case 'link/story-artefact': return storyArtefact
      case 'link/story-story': return storyStory
    }
  }

  function Crutify (spec) {
    const crut = new Crut(ssb, spec)
    const { forceUpdate, forceTombstone } = Force(crut)

    crut.forceUpdate = forceUpdate
    crut.forceTombstone = forceTombstone

    return crut
  }
}
